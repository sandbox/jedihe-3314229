(function ($, Drupal, drupalSettings) {
  'use strict';

  $.fn.lazyBlockRenderPlaceholders = function () {
    let blockIds = Array.from(document.querySelectorAll('[data-lazy-block-placeholder-id]'))
      .map(elem => elem.dataset.lazyBlockPlaceholderId);

    const prefix = drupalSettings.path.pathPrefix;
    const ajaxOpts = {
      url: `/${prefix}lazy-block/render-placeholders`,
      submit: { block_ids: blockIds }
    };
    Drupal.ajax(ajaxOpts).execute();
  };

  Drupal.behaviors.lazyBlockRender = {
    attach: function () {
      $.fn.lazyBlockRenderPlaceholders();
    }
  };

})(jQuery, Drupal, drupalSettings);
