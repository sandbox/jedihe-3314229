<?php

namespace Drupal\lazy_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the config form for lazy_block.
 */
class LazyBlockConfigForm extends ConfigFormBase {

  /**
   * The plugin.manager.condition service.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManagerCondition;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pluginManagerCondition = $container->get('plugin.manager.condition');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lazy_block_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $block_ui_definitions = $this->pluginManagerCondition->getFilteredDefinitions('block_ui');
    $options = array_combine(
      array_keys($block_ui_definitions),
      array_column($block_ui_definitions, 'label')
    );
    $form['lazy_by_plugin'] = [
      '#type' => "checkboxes",
      '#title' => $this->t("Lazy Load By Visibility Condition"),
      '#options' => $options,
      '#default_value' => $this->config('lazy_block.settings')->get('lazy_by_plugin'),
      '#description' => $this->t("Lazy loading will be enabled for blocks using any of the selected condition plugins."),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('lazy_block.settings');

    if ($form_state->hasValue('lazy_by_plugin')) {
      $config->set('lazy_by_plugin', $form_state->getValue('lazy_by_plugin'));
    }

    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['lazy_block.settings'];
  }

}
