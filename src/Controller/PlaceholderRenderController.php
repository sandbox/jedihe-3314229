<?php

namespace Drupal\lazy_block\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Renders markup for lazy_block placeholders.
 */
class PlaceholderRenderController extends ControllerBase {

  /**
   * Renders all requested blocks into an AjaxResponse.
   */
  public function render(Request $request) {
    $block_ids = $request->request->get('block_ids', []);

    $block_storage = $this->entityTypeManager()->getStorage('block');
    $block_view_builder = $this->entityTypeManager()->getViewBuilder('block');

    $rendered = array_map(function ($bid) use ($block_storage, $block_view_builder) {
      $block = $block_storage->load($bid) ?: [];
      return $block && $block->access('view', NULL, TRUE)->isAllowed() ?
        $block_view_builder->view($block) :
        ['#cache' => ['max-age' => 0]];
    }, $block_ids);
    $response = new AjaxResponse();
    foreach (array_filter(array_combine($block_ids, $rendered)) as $bid => $build) {
      $response->addCommand(new ReplaceCommand('[data-lazy-block-placeholder-id="' . $bid . '"]', $build));
    }
    return $response;
  }

}
