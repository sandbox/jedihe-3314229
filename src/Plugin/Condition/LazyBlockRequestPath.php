<?php

namespace Drupal\lazy_block\Plugin\Condition;

use Drupal\system\Plugin\Condition\RequestPath;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override for RequestPath.
 *
 * Overrides applied:
 * - When evaluating the condition for lazy-rendered blocks, check path
 *   visibility against the referer (sic) header.
 */
class LazyBlockRequestPath extends RequestPath {

  /**
   * The current_route_match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $must_pop = FALSE;
    if ($this->routeMatch->getRouteName() === 'lazy_block.render_placeholders' &&
        ($referrer = $this->requestStack->getMainRequest()->headers->get('referer'))) {

      $request_from_referrer = Request::create(parse_url($referrer)['path']);
      $this->requestStack->push($request_from_referrer);
      $must_pop = TRUE;
    }

    $result = parent::evaluate();

    if ($must_pop) {
      $this->requestStack->pop();
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    // @todo Implement custom context that resolves against any key under the
    // 'defaults' section of the route. Then, target the
    // _lazy_block_placeholder_renderer custom key we use for route
    // lazy_block.render_placeholders.
    $contexts[] = 'route';
    return $contexts;
  }

}
