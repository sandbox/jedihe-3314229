<?php

namespace Drupal\lazy_block\ContextProvider;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Routing\AccessAwareRouterInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\ContextProvider\NodeRouteContext;
use Symfony\Component\HttpFoundation\Request;

/**
 * Decorator for core's NodeRouteContext.
 */
class LazyBlockNodeRouteContext extends NodeRouteContext {

  /**
   * The decorated node.node_route_context service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextProviderInterface
   */
  protected $innerService;

  /**
   * The request_stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The router service.
   *
   * @var \Drupal\Core\Routing\AccessAwareRouterInterface
   */
  protected $router;

  /**
   * Constructs a new LazyBlockNodeRouteContext.
   *
   * @param \Drupal\Core\Plugin\Context\ContextProviderInterface $inner_service
   *   The decorated node.node_route_context service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request_stack service.
   * @param \Drupal\Core\Routing\AccessAwareRouterInterface $router
   *   The router service.
   */
  public function __construct(ContextProviderInterface $inner_service, RouteMatchInterface $route_match, RequestStack $request_stack, AccessAwareRouterInterface $router) {
    parent::__construct($route_match);
    $this->innerService = $inner_service;
    $this->requestStack = $request_stack;
    $this->router = $router;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $result = $this->innerService->getRuntimeContexts($unqualified_context_ids);
    $value = NULL;

    if (!$result['node']->hasContextValue() &&
        $this->routeMatch->getRouteName() === 'lazy_block.render_placeholders' &&
        ($referrer = $this->requestStack->getMainRequest()->headers->get('referer'))) {

      try {
        $request_from_referrer = Request::create(parse_url($referrer)['path']);
        $this->router->matchRequest($request_from_referrer);
        // Route matching may fail due to access control.
        $referrer_route_match = RouteMatch::createFromRequest($request_from_referrer);
        $referrer_route_contexts = $referrer_route_match->getRouteObject()->getOption('parameters');
      }
      catch (\Exception $e) {
        $referrer_route_contexts = [];
      }

      if (isset($referrer_route_contexts['node']) &&
          ($node = $referrer_route_match->getParameter('node'))) {
        $value = $node;
      }
    }

    if ($value) {
      $cacheability = new CacheableMetadata();
      $cacheability->setCacheContexts(['route']);

      $context_definition = EntityContextDefinition::create('node')->setRequired(FALSE);
      $context = new Context($context_definition, $value);
      $context->addCacheableDependency($cacheability);
      $result['node'] = $context;
    }

    return $result;
  }

}
